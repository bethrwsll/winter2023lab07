public class Board{
	private Square[][] tictactoeBoard;
	
	// Constructor
	public Board() {
		tictactoeBoard = new Square[3][3];
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	//Board toString
	public String toString(){
		String board = "";
		for(int i = 0; i < tictactoeBoard.length; i++){
			board += i + "  ";
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				board += tictactoeBoard[i][j] + " ";
			}
			board += "\n";
		}
		//Only hard coding because its a small board!
		return board += "   0 1 2";
	}
	
	//Instance methods
	public boolean placeToken(int row, int col, Square playerToken) {
		if((row >= 0 || row <= 2) && (col >= 0 || col <= 2)) {
			if(tictactoeBoard[row][col] == Square.BLANK){
				tictactoeBoard[row][col] = playerToken;
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public boolean checkIfFull() {
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++) {
				if(tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean checkIfWinning(Square playerToken) {
		if(checkIfWinningVertical(playerToken) || checkIfWinningHorizontal(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}
		else {
			return false;
		}
	}
	
	//Private methods
	private boolean checkIfWinningHorizontal(Square playerToken) {
		for(int i = 0; i < tictactoeBoard.length; i++) {
			if(tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken) {
		for(int i = 0; i < tictactoeBoard.length; i++) {
			if(tictactoeBoard[0][i] == playerToken && tictactoeBoard[1][i] == playerToken && tictactoeBoard[2][i] == playerToken){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken) {
		if(tictactoeBoard[0][0] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][2] == playerToken){
				return true;
			}
		else if(tictactoeBoard[0][2] == playerToken && tictactoeBoard[1][1] == playerToken && tictactoeBoard[2][0] == playerToken) {
				return true;
			}
		return false;
	}
}