import java.util.Scanner;
public class TicTacToeGame {

	public static void main(String[] args) {
		
		System.out.println();
		
		System.out.println("Welcome to Tic Tac Toe!");
		System.out.println("Player 1: X");
		System.out.println("Player 2: O");
		
		System.out.println();
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		
		//loop
		while(gameOver == false) {
			
			System.out.println(board);
			
			if(player == 1) {
				playerToken = Square.X;
			}
			else {
				playerToken = Square.O;
			}
			
			Scanner scan = new Scanner(System.in);
				
			System.out.println("Player " + player + " it's your turn! Enter your row number:");
			int row = scan.nextInt();
				
			System.out.println("Enter your column number:");
			int column = scan.nextInt();
				
				
			while(board.placeToken(row, column, playerToken) == false){
				System.out.println("Invalid value! Re-enter the row number:");
				row = scan.nextInt();
					
				System.out.println("Invalid value! Re-enter the column number:");
				column = scan.nextInt();
				
				}
				
			if(board.checkIfFull()) {
				System.out.println(board);
				System.out.println("It's a tie!");
				gameOver = true;
			} 
			else {
				if(board.checkIfWinning(playerToken)) {
					System.out.println(board);
					System.out.println("Player " +player+ " is the winner!");
					gameOver = true;
				}
				else {
					player += 1;
					if(player > 2) {
						player = 1;
					}
				}
			}
		}
	}
}